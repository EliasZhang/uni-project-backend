var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http, {
    cors: {
        origin: '*',
    }
});
var cors = require('cors');

app.use(cors());
app.options('*', cors());

app.get('/', (req, res) => {
    res.send('Successful response.');
    console.info('test')
});

io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('message', (msg) => {
        const message = {
            text: msg,
            user: {
                id: 1,
                firstName: "Fernanda",
                lastName: "Dugall",
                imageUrl: "http://dummyimage.com/100x100.png/cc0000/ffffff"
            },
            time: new Date()
        }; 
        socket.broadcast.emit('message-broadcast', message);
        socket.emit('message-broadcast', message);
    });
});

http.listen(3000, () => console.log('Example app is listening on port 3000.'));
